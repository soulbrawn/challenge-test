import { GET_SPLASH_SCREEN_TEXTS } from '../utils/Constants';
import { callGet } from '../utils/ConnectionManager';

export function getTexts(successFunction, errorFunction, finallyFunction, component) {
    callGet(GET_SPLASH_SCREEN_TEXTS, null, successFunction, errorFunction, finallyFunction, component);
}
