//Permet triar entre dialogs amb botons o sense
import React from 'react';
import PropTypes from 'prop-types';

import {
    DialogActions,
    DialogContent,
    DialogTitle,
    Button
} from '@material-ui/core';
import DialogTemplate from '@material-ui/core/Dialog';
import Notice from '../components/Notice';
import { FormattedMessage } from 'react-intl';

//TYPES
const TYPE_YES_NO = 'yes_no';
const TYPE_OK = 'ok';

const ICON = {
    INFO: 'info',
    WARNING: 'warning',
    ERROR: 'error'
}

/**
 * ==================================================
 * Props:
 * @prop open {bool} - True to set visible this component **Required.**
 * @prop type {string} - { @see TYPES } 'yes_no' 'ok' **Required.**
 * @prop [text] {string} - The title text
 * @props [title] {string} - Modal text
 * @fires [onAgree] {function} - Fired when ok button is clicked.
 * @fires [onCancel] {function} - Fired when ok button is clicked.
 * }
 * ==================================================
 */
class Modal extends React.Component {
    //STATIC CONSTS
    static get Warning() {
        return ICON.WARNING;
    }

    static get Info() {
        return ICON.INFO;
    }

    static get Error() {
        return ICON.ERROR;
    }

    static get YesNo() {
        return TYPE_YES_NO;
    }

    static get Ok() {
        return TYPE_OK;
    }

    //STATIC FUNCTIONS
    static show(props) {
        if (Modal.__singletonRef != null) {
            Modal.__singletonRef.__show(props);
        }

    }

    static hide() {
        Modal.__singletonRef.__hide();
    }
    //CONSTRUCTOR
    constructor(props) {
        super(props);

        this.state = {
            open: this.props.open
        };
        Modal.__singletonRef = this;
    }

    __show(props) {
        this.setState({
            open: true,
            title: props.title || this.props.title,
            text: props.text || this.props.text,
            onAgree: props.onAgree || function () { },
            onCancel: props.onCancel || function () { },
            type: props.type || TYPE_OK,
            icon: props.icon || null
        });
    }

    __hide = () => this._handleClose();

    //EVENTS
    _handleClose() {
        this.setState({
            open: false
        });
        if (this.state.onCancel) {
            this.state.onCancel();
        }
    }

    _handleAgree() {
        this.setState({
            open: false
        });
        if (this.state.onAgree) {
            this.state.onAgree();
        }
    }

    //EXTENDED VIEWS
    _showButtons(type) {
        var newButtons = [];
        switch (type) {
            case TYPE_YES_NO:
                newButtons.push(
                    <Button key='btn-no' onClick={() => this._handleClose()} color='default'>
                        <FormattedMessage id="NO" />
                    </Button>
                );
                newButtons.push(
                    <Button key='btn-ok' onClick={() => this._handleAgree()} color='primary'>
                        <FormattedMessage id="YES" />
                    </Button>
                );
                break;
            case TYPE_OK:
                newButtons.push(
                    <Button key='btn-ok' onClick={() => this._handleAgree()} color='primary'>
                        <FormattedMessage id="OK" />
                    </Button>
                );
                break;
            default:
                break;
        }

        return newButtons;
    }

    _showMessage(type) {
        switch (type) {
            case TYPE_YES_NO:
                return (
                    <Notice key='n-w' iconType={this.state.icon}>
                        <p>{this.state.text}</p>
                    </Notice>
                );
            case TYPE_OK:
                return (
                    <Notice key='n-w' iconType={this.state.icon}>
                        <p>{this.state.text}</p>
                    </Notice>
                );
            default:
                return;
        }
    }

    render() {
        const { type, title } = this.state;

        return (
            <DialogTemplate
                open={this.state.open}
                keepMounted
                onClose={() => this._handleClose()}
                aria-labelledby='alert-dialog-slide-title'
                aria-describedby='alert-dialog-slide-description'
            >
                {title && (<DialogTitle id='alert-dialog-slide-title'>{title}</DialogTitle>)}

                <DialogContent dividers>
                    {this._showMessage(type)}
                </DialogContent>
                <DialogActions>
                    {this._showButtons(type)}
                </DialogActions>
            </DialogTemplate>
        )
    }
}

Modal.propTypes = {
    open: PropTypes.bool.isRequired,
    text: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired
};

export default Modal;