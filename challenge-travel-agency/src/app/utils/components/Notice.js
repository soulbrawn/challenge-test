import React from "react";
import { Icon } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";

/**
 * 
 * @param {string} iconType - Possibles types: 'warning', 'info', 'error' 
 */

//icon types
const ICON_WARNING = 'warning';
const ICON_INFO = 'info';
const ICON_ERROR = 'error';

const styles = theme => (
  {
    root: {
      display: "flex",
      justifyContent: "center",
      alignItems: "flex-end"
    },
    icon_warning: {
      color: '#e6e600',
      margin: theme.spacing(0, 1, 0, 0)
    },
    icon_error: {
      color: '#e62e00',
      margin: theme.spacing(0, 1, 0, 0)
    },
    icon_info: {
      color: '#1a53ff',
      margin: theme.spacing(0, 1, 0, 0)
    }
  }
);

class Notice extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      iconType: this.props.iconType
    }
  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      iconType: props.iconType || this.props.iconType
    });
  }

  getIcon = (classes3, iconType) => {
    switch (iconType) {
      case ICON_WARNING:
        return (
          <Icon
            className={classes3.icon_warning}
            style={{ fontSize: 50 }}
          >
            warning
                  </Icon>
        );
      case ICON_INFO:
        return (
          <Icon
            className={classes3.icon_info}
            style={{ fontSize: 50 }}
          >
            info
                  </Icon>
        );
      case ICON_ERROR:
        return (
          <Icon
            className={classes3.icon_error}
            style={{ fontSize: 50 }}
          >
            error
                  </Icon>
        );
      default:
        return;
    }
  };

  render() {
    const { textRef, classes, children } = this.props;

    return (
      <div className='row' style={{ flexGrow: 1, alignItems: 'center' }}>
        {this.state.iconType && (
          <div className='col-md-2'>
            {this.state.iconType && (this.getIcon(classes, this.state.iconType))}
          </div>
        )}
        <div ref={textRef} className={this.state.iconType ? 'col-md-10' : 'col-md-12'}>
          {children}
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(Notice);

// Set display name for debugging.
if (process.env.NODE_ENV !== "production") {
  Notice.displayName = "Notice";
}
