import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'redux';
import { injectIntl } from 'react-intl';

import {
    Backdrop,
    CircularProgress
} from '@material-ui/core';
import './Mask.scss';
import { Row, Col } from 'react-bootstrap';

const style = theme => ({
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
        bgcolor: '#000'
    },
});

class Mask extends React.Component {
    //CONSTRUCTOR
    constructor(props) {
        super(props);

        this.state = {
            open: this.props.isLoading,
            text: this.props.intl.formatMessage({ id: 'MASK.LOADING' })
        }

        Mask.__singletonRef = this;
    }

    //STATIC FUNCTIONS
    static setLoading(loading) {
        if (Mask.__singletonRef != null) {
            Mask.__singletonRef.__setLoading(loading);
        }
    }

    __setLoading(loading) {
        if (typeof loading === 'string') {
            this.setState({
                open: true,
                text: loading
            });
        } else {
            this.setState({
                open: loading === false ? false : loading || true
            });
        }
    }

    render() {
        const { classes } = this.props;

        return (
            <Backdrop
                className={classes.backdrop}
                open={this.state.open}>
                <Row>
                    <Col className='loading-align'>
                        <CircularProgress color='inherit' />
                        <p>{this.state.text}</p>
                    </Col>
                </Row>
            </Backdrop>
        )
    }
}

export default compose(
    injectIntl,
    withStyles(style)
)(Mask);