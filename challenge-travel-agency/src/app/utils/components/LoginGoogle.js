import React, { useState } from "react";
import { compose } from 'redux';
import { withStyles } from '@material-ui/core/styles';
import GoogleLogin from 'react-google-login';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'

/** Component custom style definitions */
const styles = theme => ({
    dialogTitle: {
        textAlign: 'center',
        backgroundColor: '#8a9ef3',
        minHeight: '60px',
        color: 'white',
        fontWeight: 'bold'
    },
    dialogTitleIcon: {
        top: 40,
        right: '42%',
        position: 'absolute',
        maxWidth: '100px',
        fontSize: '90px',
        color: '#53b58c'
    },
    dialogContent: {
        marginTop: '60px',
        textAlign: 'center'
    },
    dialogFooter: {
        margin: '0px auto'
    },
    closeDialogIcon: {
        cursor: 'pointer',
        position: 'absolute',
        right: 20,
        top: 10
    }
});

/**
 * React Google Login implementation
 * @param {object} props 
 * @returns 
 */

function LoginGoogle(props) {

    /** LogedIn dialog transition implementation */
    const Transition = React.forwardRef(function Transition(props, ref) {
        return <Slide direction="up" ref={ref} {...props} />;
    });

    /** WithStyles classes injection */
    const classes = props.classes;
    /** Open dialog state handler */
    const [dialogOpened, setDialogOpened] = useState(false);

    /** Succes Login handler*/
    const responseGoogleSucces = (response) => {
        setDialogOpened(true);
    }

    /** Failed Login handler*/
    const responseGoogleFailure = (response) => {

    }

    /** Renderization method with WithStyles classes injected, login Google form, Material UI Dialog, etc... */
    return (
        <>
            <div className="kt-login__body">
                <div className="kt-login__form">
                    <GoogleLogin
                        clientId="991763313443-omhvgpgcp0ilk2o27dceod6ttv3knevg.apps.googleusercontent.com"
                        buttonText="Continue With Google"
                        onSuccess={responseGoogleSucces}
                        onFailure={responseGoogleFailure}
                        cookiePolicy={'single_host_origin'}
                    />
                </div>
            </div>
            <Dialog
                open={dialogOpened}
                onClose={() => this.handleCloseDialog()}
                TransitionComponent={Transition}
                keepMounted
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title" className={classes.dialogTitle}>
                    {'Thank You!'} <span className={classes.closeDialogIcon} onClick={() => setDialogOpened(false)}><FontAwesomeIcon icon={faTimes} /></span>
                    <div className={classes.dialogTitleIcon}><span><FontAwesomeIcon icon={faCheckCircle} /></span></div>
                </DialogTitle>
                <DialogContent className={classes.dialogContent}>
                    <DialogContentText id="alert-dialog-description">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tincidunt facilisis lectus id euismod. Fusce porta maximus odio at scelerisque. Nunc sit amet nisi vel mauris placerat elementum. Vivamus dictum aliquam maximus. Sed quis velit sit amet neque consequat ultrices. Donec eu sagittis purus. Phasellus finibus, sem ac porttitor ullamcorper, quam eros fermentum turpis, quis faucibus urna magna non libero. Donec molestie rutrum metus, id fermentum odio ultrices non. Suspendisse et placerat mi. Integer vitae sapien vel neque rutrum placerat. Aliquam turpis tellus, accumsan non diam et, efficitur lobortis ipsum. Suspendisse molestie in leo sed euismod. Proin molestie dapibus nibh, eget lacinia diam egestas at. Nulla ultrices pulvinar nisi nec posuere. Proin vehicula finibus mi sit amet fringilla. Mauris consectetur dui in sagittis scelerisque.
                    </DialogContentText>
                </DialogContent>
                <DialogActions className={classes.dialogFooter}>
                    <Button onClick={() => setDialogOpened(false)} color="primary" variant='outlined'>
                        Continue
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
}

export default compose(
    withStyles(styles)
)(LoginGoogle);