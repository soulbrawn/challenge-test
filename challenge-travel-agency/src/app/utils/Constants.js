/** GLOBAL CONFIGURATIONS */
export const BASE_API_URL = 'http://localhost:3000/';

/** API END POINTS */
export const GET_SPLASH_SCREEN_TEXTS = 'resources/en.json';
export const GET_HOTEL_GUEST_SCREEN_TEXTS = 'resources/en.json';