import Mask from './components/mask/Mask';
import Modal from '../utils/components/Modal';
import { BASE_API_URL } from '../utils/Constants';

/**
 * Ajax fetch call with GET http verb implementation
 * @param {string} url 
 * @param {string} request 
 * @param {function} successFunction 
 * @param {function} errorFunction 
 * @param {function} finallyFunction 
 * @param {object} component 
 */

export function callGet(url, request, successFunction, errorFunction, finallyFunction, component) {
    Mask.setLoading(true);

    fetch(composeURL(url), {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        params: request,
    })
        .then(response => checkResponseErrors(response, component))
        .then(respJson => manageResponse(respJson, successFunction, component))
        .catch(error => errorFunction ? errorFunction.bind(component)(error, component) : printError.bind(component))
        .finally(finallyFunction ? finallyFunction.bind(component)(component) : function () {
            Mask.setLoading(false);
        });
}

/**
 * Check response method where correct response is returned or error is showed in a dialog
 * @param {object} response 
 * @param {object} component 
 * @returns 
 */
function checkResponseErrors(response, component) {
    if (response.ok) {
        return response.json();
    } else {
        response.text()
            .then((error) => {
                Modal.show({
                    open: true,
                    title: 'Error',
                    text: error,
                    type: Modal.Ok,
                    icon: Modal.Error
                });
            });//statusText
    }
}

/**
 * If fecth returns some value, the succesFunction will be binded to component
 * @param {object} respJson 
 * @param {function} successFunction 
 * @param {object} component 
 * @returns 
 */
function manageResponse(respJson, successFunction, component) {
    if (!respJson) {
        return;
    }
    else {
        successFunction.bind(component)(respJson, component)
        return;
    }
}

/**
 * Method that show if an error ocurred on fetch
 * @param {string} error 
 */
function printError(error) {
    Modal.show({
        open: true,
        title: 'Error',
        text: error.message,
        type: Modal.Ok,
        icon: Modal.Error
    });
}

/**
 * Method that join the base api url with executed endpoint
 * @param {string} endpoint 
 * @returns 
 */
function composeURL(endpoint) {
    return BASE_API_URL + endpoint;
}