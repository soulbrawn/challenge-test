import React from "react";
import { getTexts } from '../gateway/HotelGuestScreen';
import { compose } from 'redux';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'

/** LogedIn dialog transition implementation */
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

/** Screen custom style definitions */
const styles = theme => ({
    header: {
        display: 'block',
        textAlign: 'center',
        width: '100vw',
        height: '2em',
        borderBottom: '1px solid #00000036',
        boxShadow: '5px 8px 23px -10px #707070',
        margin: '15px auto'
    },
    backButton: {
        float: 'left',
        marginLeft: '3em',
        cursor: 'pointer'
    },
    closeButton: {
        float: 'right',
        marginRight: '3em',
        cursor: 'pointer'
    },
    body: {
        display: 'flex',
        width: '100vw',
        height: '80vh',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    bottom: {
        display: 'flex',
        width: '100vw',
        height: '5vh',
        justifyContent: 'center',
        alignItems: 'end'
    },
    dialogTitle: {
        textAlign: 'center',
        backgroundColor: '#8a9ef3',
        minHeight: '60px',
        color: 'white',
        fontWeight: 'bold'
    },
    dialogTitleIcon: {
        top: 40,
        right: '42%',
        position: 'absolute',
        maxWidth: '100px',
        fontSize: '90px',
        color: '#53b58c'
    },
    dialogContent: {
        marginTop: '60px',
        textAlign: 'center'
    },
    dialogFooter: {
        margin: '0px auto'
    },
    closeDialogIcon: {
        cursor: 'pointer',
        position: 'absolute',
        right: 20,
        top: 10
    }
});

/**
 *  Hotel guest screen where the user can fullfill a form to login
 */
class HotelGuestScreen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dialogOpened: false,
            formFullfilled: false,
            formfields: {
                name: '',
                lastName: '',
                email: '',
                send: ''
            },
            formButtons: {
                send: ''
            },
            formValues: {
                name: '',
                lastName: '',
                email: ''
            }
        };
    }

    /** On component initialization, the ajax text load is started*/
    componentDidMount() {
        this.callGetTexts();
    }

    /** CALLS */

    /** Call to get the screen texts via ajax call passthrough the hotel guest screen gateway*/
    callGetTexts() {
        getTexts((data) => this.loadGetTexts(data), null, null, this);
    }

    /** Set in screen state the loaded texts */
    loadGetTexts(response) {
        this.setState(
            {
                formfields: response.hotelGuestScreen.form.fields,
                formButtons: response.hotelGuestScreen.form.buttons
            }
        );
    }

    /** Call to get the form values after click any form field */
    callGetValues() {
        getTexts((data) => this.loadGetValues(data), null, null, this);
    }

    /** Set in screen state the loaded form values */
    loadGetValues(response) {
        this.setState(
            {
                formValues: response.hotelGuestScreen.mockupValues,
                formFullfilled: true
            }
        );        
    }
    /** END CALLS */

    /** EVENTS */

    /** Handler of sended form that open the loged in Dialog*/
    handleSendForm() {
        this.setState(
            { dialogOpened: true }
        );
    }

    /** Handler of closing Dialog*/
    handleCloseDialog() {
        this.setState(
            { dialogOpened: false }
        );
    }

    /** Handler of clicked form field to call load form values*/
    handleClickFieldForm() {
        if (!this.state.formFullfilled) {
            this.callGetValues();
        }

    }

    /** Handler of nav back action */
    handleBack() {
        this.props.history.goBack();
    }

    /** Handler of nav close screen action */
    handleCloseScreen() {
        this.props.history.goBack();
    }
    /** END EVENTS */

    /** PRIVATE METHODS */
    /**END PRIVATEMETHODS */

    render() {
        /** Renderization method with WithStyles classes injected, login form, Material UI Dialog, etc... */
        const { classes } = this.props;
        return (
            <>
                <div className={classes.header}>
                    <div className={classes.backButton} > <span onClick={() => this.handleBack()}><FontAwesomeIcon icon={faArrowLeft} /></span></div>
                    <div className={classes.closeButton} > <span onClick={() => this.handleCloseScreen()}><FontAwesomeIcon icon={faTimes} /></span></div>

                </div>
                <div className={classes.body}>
                    <form className={classes.root} noValidate autoComplete="off">
                        <div><TextField id="standard-basic" label={this.state.formfields.name} onClick={() => this.handleClickFieldForm()} value={this.state.formValues.name} /></div>
                        <br />
                        <div><TextField id="standard-basic" label={this.state.formfields.lastName} onClick={() => this.handleClickFieldForm()} value={this.state.formValues.lastName} /></div>
                        <br />
                        <div><TextField id="standard-basic" label={this.state.formfields.email} onClick={() => this.handleClickFieldForm()} value={this.state.formValues.email} /></div>
                    </form>
                </div>
                <div className={classes.bottom}>
                    <Button className={classes.button} variant='outlined' color='primary' onClick={() => this.handleSendForm()} disabled={!this.state.formFullfilled}>
                        {this.state.formButtons.send}
                    </Button>
                </div>
                <Dialog
                    open={this.state.dialogOpened}
                    onClose={() => this.handleCloseDialog()}
                    TransitionComponent={Transition}
                    keepMounted
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title" className={classes.dialogTitle}>
                        {'Thank You!'} <span className={classes.closeDialogIcon} onClick={() => this.handleCloseDialog()}><FontAwesomeIcon icon={faTimes} /></span>
                        <div className={classes.dialogTitleIcon}><span><FontAwesomeIcon icon={faCheckCircle} /></span></div>
                    </DialogTitle>
                    <DialogContent className={classes.dialogContent}>
                        <DialogContentText id="alert-dialog-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tincidunt facilisis lectus id euismod. Fusce porta maximus odio at scelerisque. Nunc sit amet nisi vel mauris placerat elementum. Vivamus dictum aliquam maximus. Sed quis velit sit amet neque consequat ultrices. Donec eu sagittis purus. Phasellus finibus, sem ac porttitor ullamcorper, quam eros fermentum turpis, quis faucibus urna magna non libero. Donec molestie rutrum metus, id fermentum odio ultrices non. Suspendisse et placerat mi. Integer vitae sapien vel neque rutrum placerat. Aliquam turpis tellus, accumsan non diam et, efficitur lobortis ipsum. Suspendisse molestie in leo sed euismod. Proin molestie dapibus nibh, eget lacinia diam egestas at. Nulla ultrices pulvinar nisi nec posuere. Proin vehicula finibus mi sit amet fringilla. Mauris consectetur dui in sagittis scelerisque.
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions className={classes.dialogFooter}>
                        <Button onClick={() => this.handleCloseDialog()} color="primary" variant='outlined'>
                            Continue
                        </Button>
                    </DialogActions>
                </Dialog>
            </>
        );
    }
}

export default compose(
    withStyles(styles)
)(HotelGuestScreen);