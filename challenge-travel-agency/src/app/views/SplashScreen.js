import React from "react";
import { compose } from 'redux';
import { withStyles } from '@material-ui/core/styles';
import { getTexts } from '../gateway/SplashScreen';
import Button from '@material-ui/core/Button';
import {
    Link
} from "react-router-dom";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import LoginGoogle from "../utils/components/LoginGoogle";

/** LogedIn dialog transition implementation */
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

/** Screen custom style definitions */
const styles = theme => ({
    body: {
        display: 'flex',
        width: '100vw',
        height: '80vh',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    bottom: {
        textAlign: 'center',
        display: 'block',
        width: '100vw',
        height: '20vh',
        margin: '0px auto'
    },
    button: {
        minWidth: '170px',
        padding: '10px'
    },
    dialogTitle: {
        textAlign: 'center',
        backgroundColor: '#8a9ef3',
        minHeight: '60px',
        color: 'white',
        fontWeight: 'bold'
    },
    dialogTitleIcon: {
        top: 40,
        right: '42%',
        position: 'absolute',
        maxWidth: '100px',
        fontSize: '90px',
        color: '#53b58c'
    },
    dialogContent: {
        marginTop: '60px',
        textAlign: 'center'
    },
    dialogFooter: {
        margin: '0px auto'
    },
    closeDialogIcon: {
        cursor: 'pointer',
        position: 'absolute',
        right: 20,
        top: 10
    }
});

/**
 *  Main screen of application where the user can select what kind of user is loging in, including a google account login
 */
class SplashScreen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dialogOpened: false,
            formTexts: {
                title: "",
                subTitle: "",
                guestOption: "",
                agencyOption: "",
                loginLinkText: ""
            }
        };
    }

    /** On component initialization, the ajax text load is started*/
    componentDidMount() {
        this.callGetTexts();
    }

    /** CALLS */

    /** Call to get the screen texts via ajax call passthrough the splash screen gateway */
    callGetTexts() {
        getTexts((data) => this.loadGetTexts(data), null, null, this);
    }

    /** Set in screen state the loaded texts */
    loadGetTexts(response) {
        this.setState(
            { formTexts: response.splashScreen }
        );
    }
    /** END CALLS */

    /** EVENTS */

    /** Handler of Hotel Guest option selection that redirect to Hotel Guest Screen via Router history*/
    handleGuestOptionSelected() {
        this.props.history.push('/hotelGuestScreen');
    }

    /** Handler of Travel Agency option selection that redirect to Travel Agency Screen via Router history*/
    handleAgencyOptionSelected() {
        this.props.history.push('/travelAgencyScreen');
    }

    /** Handler of current registered user option selection that open the loged in Dialog*/
    handleLoginSelected() {
        this.setState(
            { dialogOpened: true }
        );
    }

    /** Handler of closing Dialog*/
    handleCloseDialog() {
        this.setState(
            { dialogOpened: false }
        );
    }
    /** END EVENTS */

    /** PRIVATE METHODS */
    /**END PRIVATE METHODS */

    render() {
        /** Renderization method with WithStyles classes injected, LoginGoogle component, Material UI Dialog, etc... */
        const { classes } = this.props;
        return (
            <>
                <div className={classes.body}>
                    <div>
                        <h1>{this.state.formTexts.title}</h1>
                    </div>
                    <br />
                    <div>
                        {this.state.formTexts.subTitle}
                    </div>
                    <br />
                    <div>
                        <Button className={classes.button} variant='outlined' color='primary' onClick={() => this.handleGuestOptionSelected()}>
                            {this.state.formTexts.guestOption}
                        </Button>
                    </div>
                    <br />
                    <div>
                        <Button className={classes.button} variant='outlined' color='primary' onClick={() => this.handleAgencyOptionSelected()}>
                            {this.state.formTexts.agencyOption}
                        </Button>
                    </div>
                    <br />
                </div>
                <div className={classes.bottom}>
                    <div><LoginGoogle /></div>
                    <br />
                    <div><Link onClick={() => this.handleLoginSelected()} to={'#'}>{this.state.formTexts.loginLinkText}</Link></div>
                </div>
                <Dialog
                    open={this.state.dialogOpened}
                    onClose={() => this.handleCloseDialog()}
                    TransitionComponent={Transition}
                    keepMounted
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title" className={classes.dialogTitle}>
                        {'Thank You!'} <span className={classes.closeDialogIcon} onClick={() => this.handleCloseDialog()}><FontAwesomeIcon icon={faTimes} /></span>
                        <div className={classes.dialogTitleIcon}><span><FontAwesomeIcon icon={faCheckCircle} /></span></div>
                    </DialogTitle>
                    <DialogContent className={classes.dialogContent}>
                        <DialogContentText id="alert-dialog-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tincidunt facilisis lectus id euismod. Fusce porta maximus odio at scelerisque. Nunc sit amet nisi vel mauris placerat elementum. Vivamus dictum aliquam maximus. Sed quis velit sit amet neque consequat ultrices. Donec eu sagittis purus. Phasellus finibus, sem ac porttitor ullamcorper, quam eros fermentum turpis, quis faucibus urna magna non libero. Donec molestie rutrum metus, id fermentum odio ultrices non. Suspendisse et placerat mi. Integer vitae sapien vel neque rutrum placerat. Aliquam turpis tellus, accumsan non diam et, efficitur lobortis ipsum. Suspendisse molestie in leo sed euismod. Proin molestie dapibus nibh, eget lacinia diam egestas at. Nulla ultrices pulvinar nisi nec posuere. Proin vehicula finibus mi sit amet fringilla. Mauris consectetur dui in sagittis scelerisque.
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions className={classes.dialogFooter}>
                        <Button onClick={() => this.handleCloseDialog()} color="primary" variant='outlined'>
                            Continue
                        </Button>
                    </DialogActions>
                </Dialog>
            </>
        );
    }
}
export default compose(
    withStyles(styles)
)(SplashScreen);