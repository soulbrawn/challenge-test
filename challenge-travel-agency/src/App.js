import SplashScreen from './app/views/SplashScreen';
import HotelGuestScreen from './app/views/HotelGuestScreen';
import TravelAgencyScreen from './app/views/TravelAgencyScreen';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

/**
 * Main Js of app
 * @returns 
 */

function App() {
  /**React Router setter to implement the navigation of app, inclouding three main routes to app main screens */
  return (
    <div>      
      <Router>
        <div>
          <Switch>
            <Route
              exact
              path="/"
              render={() => {
                return (
                  <Redirect to="/splashScreen" />
                )
              }} />
            <Route exact path="/splashScreen" component={SplashScreen} />
            <Route exact path="/hotelGuestScreen" component={HotelGuestScreen} />
            <Route exact path="/travelAgencyScreen" component={TravelAgencyScreen} />
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
